// Copyright (c) 2017, Aaimaa Web Solutions and contributors
// For license information, please see license.txt

frappe.ui.form.on('Notification for Deduction', {
        employee: function(frm) {
            console.log("frm", frm.doc.employee);
            frappe.call({
                "method": "frappe.client.get_list",
                args: {
                    doctype: "Salary Structure",
                    employees: frm.doc.employee,
                    columns: ['*'],
                    filters: [
                        ['Salary Structure', 'employee', '=', frm.doc.employee]
                    ]
                },
                callback: function(data) {
                    frappe.call({
                        "method": "frappe.client.get_list",
                        args: {
                            doctype: "Salary Detail",
                            fields: ['*'],
                            filters: [
                                ['Salary Detail', 'parent', '=', data.message[0].name]
                            ]
                        },
                        callback: function(data) {
                            console.log("data", data);
                            var amount;
                            $.each(data.message, function(index, value) {
                                if (value.salary_component == "Basic Salary") {
                                    amount = value.amount;
                                }
                                frm.set_value("basic_salary", amount);
                                frm.refresh_field('basic_salary');
                            });
                        }
                    });
                }
            });
        }
    }
);
frappe.ui.form.on('Deduction', {
    amount: function(frm, cdt, cdn) {

        var d = locals[cdt][cdn];
        var total = 0;
        frm.doc.deduction.forEach(function(d) {
            total += parseInt(d.amount);
        });
        frm.set_value('total', total);
        frm.refresh_field('total');
    }
});
