from __future__ import unicode_literals
import frappe
import json
from frappe import _
from frappe.model.document import Document
from erpnext import get_default_company
from frappe.utils import flt, today, getdate, add_years


def get_company_abbr(company):
    return frappe.db.sql("select abbr from tabCompany where name=%s",
                         company)[0][0]

def get_bool(val):
    if (val == "true"):
        return True
    else:
        return False

@frappe.whitelist()
def add_salary_structure(doc, method):
    ss = frappe.get_doc({
        "name":
        doc.employee_name,
        "doctype":
        "Salary Structure",
        "company":
        get_default_company(),
        "is_active":
        "Yes",
        "payroll_frequency":
        "Monthly",
        "is_default":
        "Yes",
        "payment_account":
        "Salary - " + get_company_abbr(get_default_company()),
        "employees": [{
            "docstatus": 0,
            "doctype": "Salary Structure Employee",
            "name": "New Salary Structure Employee 1",
            "__islocal": 1,
            "__unsaved": 1,
            "owner": frappe.session.user,
            "parent": "New Salary Structure 1",
            "parentfield": "employees",
            "parenttype": "Salary Structure",
            "idx": 1,
            "company": get_default_company(),
            "employee_name": doc.employee_name,
            "employee": doc.name,
            "from_date": today(),
            "base": 0,
            "to_date": add_years(today(), 1)
        }],
        "earnings": [{
            "docstatus": 0,
            "doctype": "Salary Detail",
            "name": "New Salary Detail 1",
            "__islocal": 1,
            "__unsaved": 1,
            "owner": frappe.session.user,
            "amount_based_on_formula": 1,
            "parent": "New Salary Structure 1",
            "parentfield": "earnings",
            "parenttype": "Salary Structure",
            "idx": 1,
            "__unedited": 0,
            "abbr": "B",
            "salary_component": "Basic",
            "amount": 0
        }, {
            "docstatus": 0,
            "doctype": "Salary Detail",
            "name": "New Salary Detail 2",
            "__islocal": 1,
            "__unsaved": 1,
            "owner": frappe.session.user,
            "amount_based_on_formula": 1,
            "parent": "New Salary Structure 3",
            "parentfield": "earnings",
            "parenttype": "Salary Structure",
            "idx": 2,
            "__unedited": 0,
            "abbr": "HRA",
            "salary_component": "HRA",
            "formula": "B * .30"
        }, {
            "docstatus": 0,
            "doctype": "Salary Detail",
            "name": "New Salary Detail 3",
            "__islocal": 1,
            "__unsaved": 1,
            "owner": frappe.session.user,
            "amount_based_on_formula": 1,
            "parent": "New Salary Structure 4",
            "parentfield": "earnings",
            "parenttype": "Salary Structure",
            "idx": 3,
            "__unedited": 0,
            "abbr": "CCA",
            "salary_component": "CCA",
            "formula": "B * .10"
        }],
        "deductions": [{
            "docstatus": 0,
            "doctype": "Salary Detail",
            "name": "New Salary Detail 1",
            "__islocal": 1,
            "__unsaved": 1,
            "owner": frappe.session.user,
            "amount_based_on_formula": 1,
            "parent": "New Salary Structure 1",
            "parentfield": "deductions",
            "parenttype": "Salary Structure",
            "idx": 1,
            "__unedited": 0,
            "abbr": "PF",
            "salary_component": "PF",
            "formula": "B *.12"
        }, {
            "docstatus": 0,
            "doctype": "Salary Detail",
            "name": "New Salary Detail 1",
            "__islocal": 1,
            "__unsaved": 1,
            "owner": frappe.session.user,
            "amount_based_on_formula": 1,
            "parent": "New Salary Structure 1",
            "parentfield": "deductions",
            "parenttype": "Salary Structure",
            "idx": 2,
            "__unedited": 0,
            "abbr": "TDS",
            "salary_component": "TDS",
            "amount": 0
        }, {
            "docstatus": 0,
            "doctype": "Salary Detail",
            "name": "New Salary Detail 5",
            "__islocal": 1,
            "__unsaved": 1,
            "owner": frappe.session.user,
            "amount_based_on_formula": 1,
            "parent": "New Salary Structure 1",
            "parentfield": "deductions",
            "parenttype": "Salary Structure",
            "idx": 3,
            "__unedited": 0,
            "abbr": "PT",
            "salary_component": "PT",
            "amount": 200
        }]
    }).insert()


@frappe.whitelist()
def set_salary_structure_for_all(doc):
    employees = frappe.get_list(
        "Employee", filters={"status": "Active"}, fields=["*"])

    for emp in employees:
        ss = frappe.get_doc({
            "name":
            emp.employee_name,
            "doctype":
            "Salary Structure",
            "company":
            get_default_company(),
            "is_active":
            "Yes",
            "payroll_frequency":
            "Monthly",
            "is_default":
            "Yes",
            "payment_account":
            "Salary - " + get_company_abbr(get_default_company()),
            "employees": [{
                "docstatus": 0,
                "doctype": "Salary Structure Employee",
                "name": "New Salary Structure Employee 1",
                "__islocal": 1,
                "__unsaved": 1,
                "owner": frappe.session.user,
                "parent": "New Salary Structure 1",
                "parentfield": "employees",
                "parenttype": "Salary Structure",
                "idx": 1,
                "company": get_default_company(),
                "employee_name": emp.employee_name,
                "employee": emp.name,
                "from_date": today(),
                "base": 0,
                "to_date": add_years(today(), 1)
            }],
            "earnings": [{
                "docstatus": 0,
                "doctype": "Salary Detail",
                "name": "New Salary Detail 1",
                "__islocal": 1,
                "__unsaved": 1,
                "owner": frappe.session.user,
                "amount_based_on_formula": 1,
                "parent": "New Salary Structure 1",
                "parentfield": "earnings",
                "parenttype": "Salary Structure",
                "idx": 1,
                "__unedited": 0,
                "abbr": "B",
                "salary_component": "Basic",
                "amount": 0
            }, {
                "docstatus": 0,
                "doctype": "Salary Detail",
                "name": "New Salary Detail 2",
                "__islocal": 1,
                "__unsaved": 1,
                "owner": frappe.session.user,
                "amount_based_on_formula": 1,
                "parent": "New Salary Structure 3",
                "parentfield": "earnings",
                "parenttype": "Salary Structure",
                "idx": 2,
                "__unedited": 0,
                "abbr": "HRA",
                "salary_component": "HRA",
                "formula": "B * .30"
            }, {
                "docstatus": 0,
                "doctype": "Salary Detail",
                "name": "New Salary Detail 3",
                "__islocal": 1,
                "__unsaved": 1,
                "owner": frappe.session.user,
                "amount_based_on_formula": 1,
                "parent": "New Salary Structure 4",
                "parentfield": "earnings",
                "parenttype": "Salary Structure",
                "idx": 3,
                "__unedited": 0,
                "abbr": "CCA",
                "salary_component": "CCA",
                "formula": "B * .10"
            }],
            "deductions": [{
                "docstatus": 0,
                "doctype": "Salary Detail",
                "name": "New Salary Detail 1",
                "__islocal": 1,
                "__unsaved": 1,
                "owner": frappe.session.user,
                "amount_based_on_formula": 1,
                "parent": "New Salary Structure 1",
                "parentfield": "deductions",
                "parenttype": "Salary Structure",
                "idx": 1,
                "__unedited": 0,
                "abbr": "PF",
                "salary_component": "PF",
                "formula": "B *.12"
            }, {
                "docstatus": 0,
                "doctype": "Salary Detail",
                "name": "New Salary Detail 1",
                "__islocal": 1,
                "__unsaved": 1,
                "owner": frappe.session.user,
                "amount_based_on_formula": 1,
                "parent": "New Salary Structure 1",
                "parentfield": "deductions",
                "parenttype": "Salary Structure",
                "idx": 2,
                "__unedited": 0,
                "abbr": "TDS",
                "salary_component": "TDS",
                "amount": 0
            }, {
                "docstatus": 0,
                "doctype": "Salary Detail",
                "name": "New Salary Detail 5",
                "__islocal": 1,
                "__unsaved": 1,
                "owner": frappe.session.user,
                "amount_based_on_formula": 1,
                "parent": "New Salary Structure 1",
                "parentfield": "deductions",
                "parenttype": "Salary Structure",
                "idx": 3,
                "__unedited": 0,
                "abbr": "PT",
                "salary_component": "PT",
                "amount": 200
            }]
        }).insert()
        frappe.msgprint(_("Salary Structure is added to all Employees"))
