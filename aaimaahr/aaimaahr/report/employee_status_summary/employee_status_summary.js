// Copyright (c) 2017, Aaimaa Web Solutions and contributors
// For license information, please see license.txt
/* eslint-disable */

frappe.query_reports["Employee Status Summary"] = {
	"filters": [
		{
			"fieldname":"from_date",
			"label": __("From Date"),
			"fieldtype": "Date",
			"reqd": 1,
			"default": frappe.datetime.year_start()
		},
		{
			"fieldname":"to_date",
			"label": __("To Date"),
			"fieldtype": "Date",
			"reqd": 1,
			"default": frappe.datetime.year_end()
		},
		{
			"fieldname":"status",
			"label": __("Employee Status"),
			"fieldtype": "Select",
			"default": "All",
			"options": ["All", "Active","Left"]

		},
		{
			"fieldname":"employment_type",
			"label": __("Employment Type"),
			"fieldtype": "Link",
			"options": "Employment Type"
		}
	]
}
